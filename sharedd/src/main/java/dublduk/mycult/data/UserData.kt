package dublduk.mycult.data

import kotlinx.serialization.Serializable

@Serializable
data class UserData(val username: String)
