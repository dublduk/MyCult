package dublduk.mycult.server

import dublduk.mycult.data.UserCredentials
import dublduk.mycult.data.UserData
import dublduk.mycult.server.db.HBaseGraphDb
import dublduk.mycult.server.db.applyUserSchema
import dublduk.mycult.server.repo.GUserRepository
import dublduk.mycult.server.repo.InMemSessionRepository
import dublduk.mycult.server.security.*
import io.javalin.Javalin
import io.javalin.http.HttpCode
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours

@ExperimentalSerializationApi
fun main() {
    val (deployment, auth, files) = loadServerConfig()
    val tokenGenerator = PasetoGenerator(
        TokenConfig(
            System.getenv("SHARED_SECRET_HEX"),
            auth.accessTokenHours.hours,
            auth.refreshTokenDays.days,
            deployment.toUrlString(),
            deployment.toUrlString()
        )
    )
    val app = Javalin.create { cfg ->
        cfg.showJavalinBanner = false
        cfg.accessManager(UserAccessManager(tokenGenerator))
    }

    val g = HBaseGraphDb.graph.apply {
        applyUserSchema()
    }.traversal()
    val userMgmt = UserManagement(GUserRepository(g), InMemSessionRepository(), Argon2Hasher(HashConfig()), tokenGenerator)

    app.post("/newacc", { ctx ->
        val (username, pswd) = Json.decodeFromString(UserCredentials.serializer(), ctx.body())
        val (accessToken, refreshToken) = userMgmt.createUser(username, pswd)
        ctx.sendTokens(accessToken, refreshToken)
    }, Role.ANYONE)
    app.post("/login", { ctx ->
        val (username, pswd) = Json.decodeFromString(UserCredentials.serializer(), ctx.body())
        val (accessToken, refreshToken) = userMgmt.login(username, pswd)
        ctx.sendTokens(accessToken, refreshToken)
    }, Role.ANYONE)
    app.post("/refresh_tokens", { ctx ->
        val refreshToken = ctx.findRefreshToken()
        if (refreshToken != null) {
            val (newAccessToken, newRefreshToken) = userMgmt.refreshTokens(refreshToken)
            ctx.sendTokens(newAccessToken, newRefreshToken)
        } else {
            ctx.sendUnauthorized()
        }
    }, Role.ANYONE)
    app.get("/u/settings", { ctx ->
        val userSettings = userMgmt.userSettings(ctx.getUserId())
        ctx.result(Json.encodeToString(UserData.serializer(), userSettings))
    }, Role.USER)
    val fileMgmt = FileManagement(files.uploadTo)
    app.post("/upload", { ctx ->
        ctx.uploadedFiles(files.name).forEach { file ->
            fileMgmt.upload(ctx.getUserId(), file.filename, file.content)
        }
    }, Role.USER)
    app.get("/u/{userId}/uploads/{filename}", { ctx ->
        fileMgmt.download(ctx.pathParam("userId"), ctx.pathParam("filename"))
            .copyTo(ctx.res.outputStream)
    }, Role.USER)

    app.exception(TokenExpiredException::class.java) { _, ctx ->
        ctx.sendUnauthorized()
    }.exception(BadTokenException::class.java) { _, ctx ->
        ctx.sendUnauthorized()
    }.exception(WrongPasswordException::class.java) { _, ctx ->
        ctx.status(HttpCode.FORBIDDEN)
    }.exception(UserNotFoundException::class.java) { _, ctx ->
        ctx.status(HttpCode.NOT_FOUND)
    }.exception(FileNotFoundException::class.java) { _, ctx ->
        ctx.status(HttpCode.NOT_FOUND)
    }

    app.start(deployment.host, deployment.port)
}
