package dublduk.mycult.server

import io.javalin.core.util.FileUtil
import java.io.InputStream
import java.nio.file.Paths
import kotlin.io.path.exists
import kotlin.io.path.inputStream

class FileManagement(private val uploadToPath: String) {
    fun upload(userId: String, filename: String, content: InputStream) {
        FileUtil.streamToFile(content, "$uploadToPath/$userId/uploads/$filename")
    }

    fun download(userId: String, filename: String): InputStream {
        val filePath = Paths.get("$uploadToPath/$userId/uploads/$filename")
        if (filePath.exists()) {
            return filePath.inputStream()
        } else {
            throw FileNotFoundException("'$filename' not found.")
        }
    }
}

class FileNotFoundException(message: String) : RuntimeException(message)
