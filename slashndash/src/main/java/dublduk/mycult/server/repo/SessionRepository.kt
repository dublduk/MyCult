package dublduk.mycult.server.repo

interface SessionRepository {
    fun add(userId: String, refreshToken: String)

    fun findUserIdByToken(refreshToken: String): String?

    fun replace(userId: String, oldRefreshToken: String, newRefreshToken: String)
}
