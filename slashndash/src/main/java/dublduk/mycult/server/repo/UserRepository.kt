package dublduk.mycult.server.repo

import dublduk.mycult.server.model.User

interface UserRepository {
    fun add(username: String, pswd: String): String

    fun findById(id: String): User?

    fun findByName(username: String): User?

    fun renameUser(id: String, newUsername: String): User

    fun listAll(): List<User>
}
