package dublduk.mycult.server.db

import dublduk.mycult.server.model.User
import org.apache.tinkerpop.gremlin.structure.Element
import org.apache.tinkerpop.gremlin.structure.Vertex
import org.apache.tinkerpop.gremlin.structure.VertexProperty
import org.janusgraph.core.JanusGraph
import org.janusgraph.core.PropertyKey
import org.janusgraph.core.schema.JanusGraphIndex
import org.janusgraph.core.schema.JanusGraphManagement

private const val STRING_TYPE = "string"
private const val USERNAME_IDX_KEY = "usernameIdx"

fun JanusGraph.applyUserSchema() {
    val mgmt = openManagement()
    val usernameKey = mgmt.getOrCreatePropertyKey(User.USERNAME_KEY, STRING_TYPE, VertexProperty.Cardinality.single)
    mgmt.getOrCreatePropertyKey(User.PSWD_KEY, STRING_TYPE, VertexProperty.Cardinality.single)
    mgmt.getOrCreateCompositeIndex(USERNAME_IDX_KEY, Vertex::class.java, usernameKey)
    mgmt.commit()
}

fun JanusGraphManagement.getOrCreateCompositeIndex(
    idxName: String,
    elementType: Class<out Element>,
    propertyKey: PropertyKey
): JanusGraphIndex {
    if (containsGraphIndex(idxName)) {
        return getGraphIndex(idxName)
    }
    val idxBuilder = buildIndex(idxName, elementType)
    return idxBuilder.addKey(propertyKey).buildCompositeIndex()
}
