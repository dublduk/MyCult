package dublduk.mycult.server.db

import org.janusgraph.core.JanusGraph
import org.janusgraph.core.JanusGraphFactory

object HBaseGraphDb {
    val graph: JanusGraph by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        val janusGraph = JanusGraphFactory.build()
            .set("storage.backend", "hbase")
            .open()
        janusGraph
    }
}
