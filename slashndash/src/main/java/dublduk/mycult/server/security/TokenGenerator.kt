package dublduk.mycult.server.security

interface TokenGenerator {
    fun generateTokens(userId: String): Pair<Token, Token>

    fun verifyAccessToken(accessToken: String): String

    fun verifyRefreshToken(refreshToken: String)
}
