package dublduk.mycult.server.security

import de.mkammerer.argon2.Argon2
import de.mkammerer.argon2.Argon2Factory

class Argon2Hasher(private val hashConfig: HashConfig) : PswdHasher {
    private val argon2: Argon2 = Argon2Factory.create(hashConfig.saltLength, hashConfig.hashLength)

    override fun hash(pswd: String): String {
        val pswdChars = pswd.toCharArray()
        try {
            return argon2.hash(
                hashConfig.iterations + 10,
                hashConfig.memory * 1024,
                hashConfig.parallelism,
                pswdChars
            )
        } finally {
            argon2.wipeArray(pswdChars)
        }
    }

    override fun verify(pswdHash: String, candidate: String): Boolean {
        val pswdChars = candidate.toCharArray()
        try {
            return argon2.verify(pswdHash, pswdChars)
        } finally {
            argon2.wipeArray(pswdChars)
        }
    }
}
