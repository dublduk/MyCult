package dublduk.mycult.server.security

data class Token(val name: String, val value: String, val maxAge: Int)
