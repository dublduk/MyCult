package dublduk.mycult.server.security

class TokenExpiredException(exception: Exception) : RuntimeException(exception)
