package dublduk.mycult.server.security

import kotlin.time.Duration

data class TokenConfig(
    val sharedSecretHex: String,
    val accessTime: Duration,
    val refreshTime: Duration,
    val audience: String,
    val issuer: String
)
