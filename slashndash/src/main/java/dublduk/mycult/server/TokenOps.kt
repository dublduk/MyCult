package dublduk.mycult.server

import dublduk.mycult.data.TokenData
import dublduk.mycult.server.security.Token
import io.javalin.http.Context
import io.javalin.http.Cookie
import kotlinx.serialization.json.Json

const val ACCESS_TOKEN_KEY = "access_token"
const val REFRESH_TOKEN_KEY = "refresh_token"
const val SET_COOKIE_HEADER_KEY = "Set-Cookie"
const val AUTHORIZATION_HEADER_KEY = "Authorization"
const val INITIATOR_HEADER_KEY = "X-Initiator"
const val APP_INITIATOR_VALUE = "MyCult"

fun Context.findAccessToken(): String? {
    return findTokenByKey(ACCESS_TOKEN_KEY)
}

fun Context.findRefreshToken(): String? {
    return findTokenByKey(REFRESH_TOKEN_KEY)
}

private fun Context.findTokenByKey(tokenKey: String): String? {
    if (isApplicationRequest()) {
        return header(AUTHORIZATION_HEADER_KEY)?.substring("Bearer ".length)?.trim()
    } else { // browser request
        return cookie(tokenKey)
    }
}

fun Context.sendTokens(accessToken: Token, refreshToken: Token) {
    if (isApplicationRequest()) {
        result(Json.encodeToString(TokenData.serializer(), TokenData(accessToken.value, refreshToken.value)))
    } else { // browser request
        sendTokenCookies(accessToken, refreshToken)
    }
}

private fun Context.isApplicationRequest(): Boolean = header(INITIATOR_HEADER_KEY) == APP_INITIATOR_VALUE

private fun Context.sendTokenCookies(accessToken: Token, refreshToken: Token) {
    res.addHeader(SET_COOKIE_HEADER_KEY, renderCookie(accessToken))
    res.addHeader(SET_COOKIE_HEADER_KEY, renderCookie(refreshToken))
    cookie(Cookie("logged_in", "true", maxAge = accessToken.maxAge, secure = true))
}

private fun renderCookie(token: Token) = "${token.name}=${token.value}; Secure; HTTPOnly; Max-Age=${token.maxAge}"
