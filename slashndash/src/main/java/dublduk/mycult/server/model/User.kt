package dublduk.mycult.server.model

data class User(val id: String, val username: String, val pswdHash: String) {
    companion object {
        const val MODEL_KEY = "user"
        const val ID_KEY = "id"
        const val USERNAME_KEY = "username"
        const val PSWD_KEY = "pswd"
    }
}
