# Server app

## How to run

### Run the database
Use either Hbase or Cassandra, not both.

### Run HBase in local mode
1. Use [this guide](https://hbase.apache.org/book.html#quickstart) to install HBase.
2. Run JanusGraph `docker run -it -p 8182:8182 janusgraph/janusgraph`.

### Run Cassandra in local mode
0. Replace `HBaseGraphDb` with `CassandraGraphDb` in the main function.
1. Run Cassandra `docker run -d -e CASSANDRA_START_RPC=true -p 9160:9160 -p 9042:9042 -p 7199:7199 -p 7001:7001 -p 7000:7000 cassandra:3.11`.
2. Run JanusGraph `docker run -it -p 8182:8182 janusgraph/janusgraph`.

### Run application
- Set "SHARED_SECRET_HEX" env var `export SHARED_SECRET_HEX=...`
- Run `./gradlew :slashndash:assembleDist` to build the distribution.
- Extract the resulting archive located in the `slashndash/build/distributions` directory.
- Run the `sh`/`bat` script located in the `bin` directory of the extracted archive.

---
### Useful resources
- [JanusGraph docs](https://docs.janusgraph.org/)
- [Tinkerpop/Gremlin site](https://tinkerpop.apache.org/) and [docs](https://tinkerpop.apache.org/docs/current/)
- ["Practical Gremlin"](https://www.kelvinlawrence.net/book/Gremlin-Graph-Guide.html)
